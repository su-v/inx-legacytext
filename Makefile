NAME = inx-legacytext

docs = \
	COPYING \
	GPL2.txt \
	GPL3.txt \
	README.legacytext.txt

source = \
	 src/fix_legacy_text.inx \
	 src/fix_legacy_text.py

data = 

ChangeLog.txt : $(docs) $(source) $(data)
	rm -f $@
	git log --oneline > $@

all = ChangeLog.txt $(docs) $(source) $(data)

revno = $(shell git rev-parse --short HEAD)
tag = $(shell git tag | tail -1)

.PHONY:	list
list:	$(all)
	@for i in $(all); do \
		echo $$i; \
	done

.PHONY:	zip
zip:	$(all)
	zip "$(NAME)-$(revno).zip" -X $(all)
	@echo "$(NAME)-$(revno).zip"

.PHONY: release
release: \
	$(all)
	zip "$(NAME)-$(tag).zip" -X $(all)
	@echo "$(NAME)-$(tag).zip"
